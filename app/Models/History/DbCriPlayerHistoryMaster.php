<?php

namespace App\Models\History;

use Illuminate\Database\Eloquent\Model;

class DbCriPlayerHistoryMaster extends Model {

    protected $table = 'cri_player_history_master';
    
	protected $primaryKey = 'pk_cri_player_history';
    
	protected $fillable = [
		'fk_cri_player', 'matches', 
		'runs', 'highest_score', 'fifties', 
		'hundreds', 'created_at', 'updated_at'
	];

    public function __construct() {
        
    }
}
