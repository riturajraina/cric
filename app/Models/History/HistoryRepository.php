<?php

namespace App\Models\History;

use App\Models\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryRepository extends BaseRepository {

    protected $_dbCriPlayerHistoryMaster;

    public function __construct() {
        $this->_dbCriPlayerHistoryMaster = new DbCriPlayerHistoryMaster();
    }
    
    public function fetchPlayerHistory($playerId)
    {
        try {
            
            $result =   $this->_dbCriPlayerHistoryMaster::where(['fk_cri_player' => $playerId])
                        ->get()->toArray();
            
            if (!empty($result)) {
                return $result['0'];
            }
            
            $this->error    =   'Player history not found';
            
            return [];
            
        } catch (\Exception $ex) {
            $this->setError('fetch player history', $ex);
            return false;
        }
    }
}
