<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;

class DbCriTeamMaster extends Model {

    protected $table = 'cri_team_master';
    protected $primaryKey = 'pk_cri_team';
    protected $fillable = ['identifier', 'name', 'logo_uri', 'club_state', 'created_at', 'updated_at', 'status'];

    public function __construct() {
        
    }

}
