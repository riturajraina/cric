<?php

namespace App\Models\Team;

use App\Models\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamRepository extends BaseRepository {

    protected $_dbCriTeamMaster;

    public function __construct() {
        $this->_dbCriTeamMaster = new DbCriTeamMaster();
    }

    public function fetchTeams()
    {
        try {
           $result  =   $this->_dbCriTeamMaster;
                    
           $result  =   $result->paginate(env('RECORDS_PER_PAGE'));
           
           if (count($result)) {
               return $result;
           }
           
           $this->error =   'No teams found';
           
           return [];
        } catch (Exception $ex) {
            $this->setError('Unable to fetch teams due to this exception', $ex);
            return false;
        }
    }
    
    public function fetchTeamDetails($teamId)
    {
        try {
            $result = $this->_dbCriTeamMaster::where(['pk_cri_team' => $teamId])
                      ->get()->toArray();
            
            if (count($result)) {
                return $result['0'];
            }
            
            $this->error = 'Team details not found';
            
            return [];
        } catch (\Exception $ex) {
            $this->setError('fetch team details', $ex);
            return false;
        }
    }
}
