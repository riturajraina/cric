<?php

namespace App\Models\Player;

use App\Models\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlayerRepository extends BaseRepository {

    protected $_dbCriPlayerMaster;

    public function __construct() {
        $this->_dbCriPlayerMaster = new DbCriPlayerMaster();
    }
    
    public function fetchPlayers($teamId)
    {
        try {
            
            $result = $this->_dbCriPlayerMaster::where(['fk_cri_team' => $teamId]);
            
            $result  =   $result->paginate(env('RECORDS_PER_PAGE'));
           
            if (count($result)) {
                return $result;
            }
           
            $this->error =   'No players found for this team';
           
            return [];
        } catch (\Exception $ex) {
            $this->setError('Unable to fetch players due to this error', $ex);
            return false;
        }
    }
    
    public function fetchPlayerDetails($playerId)
    {
        try {
            $result =   $this->_dbCriPlayerMaster::where(['pk_cri_player' => $playerId]);
            
            $result =   $result->get()->toArray();
            
            if (!empty($result)) {
                return $result['0'];
            }
            
            $this->error    =   'Player not found';
            
            return false;
            
        } catch (\Exception $ex) {
            $this->setError('fetch player details', $ex);
            return false;
        }
    }
    
    
}
