<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

class DbCriPlayerMaster extends Model {

    protected $table = 'cri_player_master';
    protected $primaryKey = 'pk_cri_player';
    protected $fillable = ['identifier', 'first_name', 'last_name', 'image_uri', 'jersey_number', 
	'fk_cri_team', 'created_at', 'updated_at', 'status'];

    public function __construct() {
        
    }

}
