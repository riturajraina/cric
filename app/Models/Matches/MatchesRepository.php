<?php

namespace App\Models\Matches;

use App\Models\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MatchesRepository extends BaseRepository {

    protected $_dbCriMatchesMaster;

    public function __construct() {
        $this->_dbCriMatchesMaster = new DbCriMatchesMaster();
    }
    
    public function fetchMatches($data)
    {
        try {
            
            $whereArray =   [
                'fk_cri_team_first'     =>  $data['firstTeamId'],
                'fk_cri_team_second'    =>  $data['secondTeamId'],
            ];
            
            $result     =   $this->_dbCriMatchesMaster::where($whereArray);
            
            $whereArray =   [
                'fk_cri_team_first'     =>  $data['secondTeamId'],
                'fk_cri_team_second'    =>  $data['firstTeamId'],
            ];
            
            $result     =   $result->orWhere($whereArray);
            
            $result     =   $result->paginate(env('RECORDS_PER_PAGE'));
           
            if (count($result)) {
                return $result;
            }
           
            $this->error =   'No matches found';
           
            return [];
        } catch (\Exception $ex) {
            $this->setError('fetch matches', $ex);
            return false;
        }
    }
}
