<?php

namespace App\Models\Matches;

use Illuminate\Database\Eloquent\Model;

class DbCriMatchesMaster extends Model {

    protected $table = 'cri_matches_master';
    
	protected $primaryKey = 'pk_matches_id';
    
	protected $fillable = [
		'fk_cri_team_first', 'fk_cri_team_second', 
		'first_team_point', 'second_team_point', 'fk_cri_team', 
		'created_at', 'updated_at', 'status'
	];

    public function __construct() {
        
    }
}
