<?php
namespace App\Http\Controllers\Auth;

use App\Exceptions;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Helpers\SelectOptionsManager;
use App\Helpers\EmailMobileValidator;
use App\Helpers\DateValidator;
use App\Helpers\CaptchaManager;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    protected $error;
    protected $selectOptionsManager;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
        
        parent::__construct();

        $this->error = '';
        
        $this->selectOptionsManager = new SelectOptionsManager();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {

        $validatorArray = array
        (
            'fname'         => 'required|max:50',
            'lname'         => 'required|max:50',
            'password'      => 'required|max:20',
            'confpassword'  => 'required|max:20',
            'username'		=> 'required|max:100|unique:cri_user_master',
            'captcha'       => 'required|max:5',
        );

        return Validator::make($data, $validatorArray);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) 
    {
        try {
            
            $dateTime                   =   date('Y-m-d H:i:s');
            
            User::create([
                'fname'                 => $data['fname'],
                'lname'                 => $data['lname'],
                'password'              => bcrypt($data['password']),
                'username'				=> $data['username'],
                'created_at'            => $dateTime,
                'updated_at'            => $dateTime,
            ]);
            
            return true;
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return false;
        }
    }


    public function showRegistrationForm() {
        return view('auth.register', ['selectOptionsManager' => $this->selectOptionsManager]);
    }
    

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {

        $data = $request->all();

        $errors = array();

        $captchaManager = new CaptchaManager;

        $captchaVerified = $captchaManager->VerifyCaptcha($request->captcha);

        if (!$captchaVerified) {
            $errors[] = 'Characters entered don\'t match with the image displayed';
        }

		if (trim($data['password']) <> trim($data['confpassword'])) {
			$errors[] = 'Password & Confirm password values don\'t match';
		}

        if (count($errors)) {
            return redirect('/register')
                            ->withInput()
                            ->withErrors($errors);
        }

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return redirect('/register')
                            ->withInput()
                            ->withErrors($validator);
        }

		if (!$this->create($data)) {
			return redirect('/register')
                            ->withInput()
                            ->withErrors(['Some problem with registration. Please try again']);
		}
        
        
        return redirect('/success/' . base64_encode('Registration successfull.'));
    }
}
