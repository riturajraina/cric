<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Models\Team\TeamRepository;

use App\Models\Matches\MatchesRepository;


class MatchesController extends Controller {
    
    protected $teamRepository;
    
    protected $matchesRepository;
    

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->teamRepository   =   new TeamRepository();
        $this->matchesRepository=   new MatchesRepository();
    }

    
    public function viewMatches(Request $request)
    {
        try {
            
            $data               =   $request->all();
            
            $teamListArray      =   $this->teamRepository->fetchTeams();
            
            $teamList           =   [];
            
            foreach ($teamListArray as $row) {
                $teamList[$row['pk_cri_team']] =   $row['name'];
            }
            
            $matchList  =   [];
            
            if (!empty($data)) {
                
                if (empty($data['firstTeamId']) || empty($data['secondTeamId'])) {
                    
                    return redirect('/matches')
                            ->withInput()
                            ->withErrors(['Please select both teams']);
                }
                
                if ($data['firstTeamId'] == $data['secondTeamId']) {
                    
                    return redirect('/matches')
                            ->withInput()
                            ->withErrors(['Please select different teams']);
                }
                
                $matchesData    =   [
                    'firstTeamId'   =>  $data['firstTeamId'],
                    'secondTeamId'  =>  $data['secondTeamId'],
                ];
                
                $matchList  =   $this->matchesRepository->fetchMatches($matchesData);
                
                if (!$matchList) {
                    return redirect('/matches')
                            ->withInput()
                            ->withErrors([$this->matchesRepository->getError()]);
                }
            }
            
            $firstTeamId    =   !empty($data['firstTeamId']) 
                                ? $data['firstTeamId']
                                : 
                                (
                                    !empty(Input::old('firstTeamId')) 
                                    ? Input::old('firstTeamId') :
                                    null
                                );
            
            $secondTeamId   =   !empty($data['secondTeamId']) 
                                ? $data['secondTeamId']
                                : 
                                (
                                    !empty(Input::old('secondTeamId')) 
                                    ? Input::old('secondTeamId') :
                                    null
                                );
            
            $viewArray  =   [
                'teamList'              =>  $teamList,
                'selectOptionsManager'  =>  $this->selectOptionsManager,
                'matchList'             =>  $matchList,
                'firstTeamId'           =>  $firstTeamId,
                'secondTeamId'          =>  $secondTeamId,
                
            ];
        
            return view('viewmatches', $viewArray);
            
        } catch (\Exception $ex) {
            return redirect('/goterror/' . base64_encode($ex->getMessage()));
        }
    }
}


