<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Models\Team\TeamRepository;

use App\Models\Player\PlayerRepository;

use App\Models\Matches\MatchesRepository;

use App\Models\History\HistoryRepository;

class PlayerController extends Controller {
    
    protected $teamRepository;
    
    protected $playerRepository;
    
    protected $historyRepository;
    

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->teamRepository   =   new TeamRepository();
        $this->playerRepository =   new PlayerRepository();
        $this->historyRepository=   new HistoryRepository();
    }

    
    public function viewPlayers($teamId)
    {
        try {
            $teamDetails    =   $this->teamRepository->fetchTeamDetails($teamId);
            
            if (!$teamDetails) {
                return redirect('/goterror/' . base64_encode($this->teamRepository->getError()));
            }
            
            $playerList     =   $this->playerRepository->fetchPlayers($teamId);
            
            if (!$playerList) {
                return redirect('/goterror/' . base64_encode($this->playerRepository->getError()));
            }
            
            return view('players', ['team' => $teamDetails['name'], 
                                    'playerList' => $playerList]);
        } catch (\Exception $ex) {
            return redirect('/goterror/' . base64_encode($ex->getMessage()));
        }
    }
    
    public function viewPlayerHistory($playerId)
    {
        try {
            
            $playerDetails  =   $this->playerRepository->fetchPlayerDetails($playerId);
            
            if (!$playerDetails) {
                return redirect('/goterror/' . base64_encode($this->playerRepository->getError()));
            }
            
            $player =   $playerDetails['first_name'] . ' ' . $playerDetails['last_name'];
            
            $playerHistory  =   $this->historyRepository->fetchPlayerHistory($playerId);
            
            if (!$playerHistory) {
                return redirect('/goterror/' . base64_encode($this->historyRepository->getError()));
            }
            
            $viewArray  =   ['playerHistory' => $playerHistory, 'player' => $player];
            
            return view('playerhistory', $viewArray);
            
        } catch (\Exception $ex) {
            return redirect('/goterror/' . base64_encode($ex->getMessage()));
        }
    }
}


