<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\GenerateRandom;
use App\Helpers\DateValidator;
use App\Helpers\SelectOptionsManager;
use App\Helpers\RadioOptionsManager;
use App\Helpers\CheckboxManager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $error = null;
    
    protected $generateRandom;
    
    protected $dateValidator;
    
    protected $selectOptionsManager;
    
    protected $radioOptionsManager;

    protected $checkboxManager;
    
    
    public function __construct() {
        $this->generateRandom = new GenerateRandom();
        $this->dateValidator = new DateValidator();
        $this->selectOptionsManager = new SelectOptionsManager();
        $this->radioOptionsManager = new RadioOptionsManager();
        $this->checkboxManager      = new CheckboxManager();
        $this->uploadedImages       =   [];
    }

    public function sanitizeInput($data)
    {
            array_walk($data, array($this, 'sanitize'));
            return array_filter($data);
    }

    public function sanitize(&$value, $key)
    {
        if (!is_array($value)) {
            $value = trim(addslashes(stripslashes($value)));
        }    
    }

    public function checkIfLoggedIn()
    {   
        if (!empty(session('userDetails'))) {
            return true;
        }
        
        return false;
    }
    
    public function getErrorUrl($errorMessage)
    {
        return env('APP_URL') . '/goterror/' . base64_encode($errorMessage);
    }
    
    public function getSuccessUrl($message) 
    {
        return env('APP_URL') . '/success/' . base64_encode($message);
    }
    
    
    
    public function setError($message, \Exception $ex) {
        $exceptionMessage = $ex->getMessage();
        
        if (env('APP_DEBUG') && !empty($exceptionMessage)) {
            $message = 'Unable to ' . $message . ' due to this error : ' 
                    . $ex->getMessage() 
                    . '. Please contact system administrator';
        } else {
            $message = 'Unable to ' . $message . ' due to an error. '
                    . 'Please contact system administrator';
        }
        
        $this->error .= (!empty($this->error) ? $this->newLineChar : '') . $message;
        
        return true;
    }
}

