<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\Models\Team\TeamRepository;


class HomeController extends Controller {
    
    protected $teamRepository;
    
    

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->teamRepository   =   new TeamRepository();
    }

    public function home() {
        try {
        
            $teamList   =   $this->teamRepository->fetchTeams();
        
            return view('home', ['teamList' => $teamList]);
            
        } catch (\Exception $ex) {
            
            return redirect('/goterror/' . base64_encode($ex->getMessage()));
        }
    }
}



