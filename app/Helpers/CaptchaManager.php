<?php namespace App\Helpers;
use Illuminate\Http\Request;

	class CaptchaManager
	{
		protected	$CaptchaChars;
		protected	$Code;
		protected	$im;
		protected	$CaptchaImageSource;
		protected	$HiddenFieldName;
		protected	$CaptchaSessionField;
		protected	$FirstMathVar;
		protected	$SecondMathVar;
		protected	$ResultMathVar;
		protected	$fontFile;
		public		$SessionManager;
		private static $Instance ;
		

		public function __construct($Height=100,$Width=30)
		{
			$this->HiddenFieldName		=	'HiddenCaptchaField';
			$this->CaptchaImageSource	=	'img.php';
			$this->fontFile				=	base_path() . '/' . 'app/Helpers/arial.ttf';
			$this->CaptchaSessionField	=	'FullonCaptcha';

			
			$this->CaptchaChars			= array('1', '2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','Y','U','V','W','X','Y','Z');


			$this->Code					=	session($this->CaptchaSessionField);

			if(!$this->Code)
			{
				for($i = 0; $i < 5; $i++)
				{
					$rand_char	= rand(0, (count($this->CaptchaChars) -1));
					$this->Code .= $this->CaptchaChars[$rand_char];
				}
				session([$this->CaptchaSessionField => $this->Code]);
			}
		
		}

		public function GenerateNewCaptchaCode()
		{
			$this->Code	=	'';

			for($i = 0; $i < 5; $i++)
			{
				$rand_char	= rand(0, (count($this->CaptchaChars) -1));
				$this->Code .= $this->CaptchaChars[$rand_char];
			}

			session([$this->CaptchaSessionField => $this->Code]);
		}

		public function imagepalettetotruecolor(&$img)
		{
			if (!imageistruecolor($img))
			{
				$w = imagesx($img);
				$h = imagesy($img);
				$img1 = imagecreatetruecolor($w,$h);
				imagecopy($img1,$img,0,0,0,0,$w,$h);
				$img = $img1;
			}
		}

		public function OutputImage($Height=100,$Width=100)
		{
			$this->im	= imagecreate($Height,$Width);

			$bg_color = imagecolorallocate($this->im, 255, 255, 255);
			$font_color = imagecolorallocate($this->im, 0, 111, 255);


			$characters = preg_split("//", $this->Code, -1, PREG_SPLIT_NO_EMPTY);
			
			$x = 5;
			
			foreach($characters as $char)
			{
			
				$y = rand(20,20);
				$size = rand(8,14);
				$rotation = rand(-8,8);
				imagettftext($this->im, $size, $rotation, $x, $y, $font_color, $this->fontFile, $char);
				$x += 19;
			}
			
			
			
			$this->imagepalettetotruecolor($this->im);
			
			$imgh = imagesy($this->im);
			$imgw = imagesx($this->im);
			$yrand = mt_rand(30,60);
			$xrand = mt_rand(20,40);

			header("Content-type: image/png");
			imagepng($this->im);
			imagedestroy($this->im);
		}

		

		public function ShowHiddenFieldName($Get = false)
		{
			if(!$Get)
			{
				echo $this->HiddenFieldName;
			}
			else
			{
				return $this->HiddenFieldName;
			}
			
		}

		public function GetCaptchaCode($Show = false)
		{
			if(!$Show)
			{
				return $this->Code;
			}
			echo $this->Code;
		}

		public function ShowHiddenCaptchaField($Get = false)
		{
			$Html = '<input type="hidden" name="'.$this->HiddenFieldName.'" value="'.$this->Code.'">';

			if(!$Get)
			{
				echo $Html;
			}
			else
			{
				return $Html;
			}
		}

		public function VerifyCaptcha($Value,$Type="image")
		{
			if(strtoupper($Type) == 'IMAGE')
			{
				$SessionCaptcha = session($this->CaptchaSessionField);

				if($SessionCaptcha <> '')
				{
					//$this->SessionManager->UnsetSessionElement($this->CaptchaSessionField);

					if($Value <> $SessionCaptcha)
					{
						return false;
					}

					return true;
				}

				if(@$_REQUEST[$this->HiddenFieldName] == $Value)
				{
					return true;
				}

				return false;
			}
			else
			{
				$SessionCaptcha	=	session('ResultMathVar');

				if($SessionCaptcha <> '')
				{
					$this->SessionManager->UnsetSessionElement('ResultMathVar');

					if($Value <> $SessionCaptcha)
					{
						return false;
					}

					return true;
				}

				return false;
			}
		}
	}

?>