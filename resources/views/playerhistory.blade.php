@extends('layouts.app')

@section('content')
@include('errors.common')
<?php
    $numberOfcolumns = 1;
?>
<div class="container" style="width:100%;">
    <div class="row"><!--style="width: 1500px;"-->
        <div class="col-md-30">
            <div class="alert alert-success">
                <table width="100%" align="center" border = "1" style="border-collapse:separate;border-spacing:2px;" 
                rules = "none" cellspacing="2" cellpadding="4" rules="1">
                    <?php
                        if (!empty($playerHistory)) {
                    ?>
                            <tr>
                                <td colspan="<?php echo $numberOfcolumns;?>" align="center" 
                                    style="background-color: #2579A9;text-align: center;color:#E2FFFF" width="100%">
                                    <h1>
                                        Player History - <?php echo $player;?>
                                    </h1>
                                </td>
                            </tr>

                            <tr style="background-color:white">
                                <td align="center" colspan="<?php echo $numberOfcolumns;?>">
                                    <table width="100%" align="center" border = "1" 
                                    style="border-collapse:separate;border-spacing:2px;" 
                                    rules = "none" cellspacing="2" cellpadding="4" rules="1">
                                        <tr>
                                            <td width="50%" align="right">
                                                <strong>Matches : </strong>
                                            </td>
                                            <td width="50%" aligh="left">
                                                <?php
                                                    echo $playerHistory['matches'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Runs : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['runs'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Highest Score : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['highest_score'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Fifties : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['fifties'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Hundreds : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['hundreds'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Created At : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['created_at'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right">
                                                <strong>Updated At : </strong>
                                            </td>
                                            <td aligh="left">
                                                <?php
                                                    echo $playerHistory['updated_at'];
                                                ?>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="2" align="center">
                                                <input type="button" value="Back"
                                                       onclick="javascript:history.go(-1);">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    <?php
                        } else {
                    ?>      <tr>
                                <td colspan="<?php echo $numberOfcolumns;?>" align="center" 
                                    style="background-color: #2579A9;text-align: center;color:#E2FFFF" width="100%">
                                    <h1>
                                        No history found
                                    </h1>
                                </td>
                            </tr>
                    <?php
                        }
                    ?>
                    
                    
                </table>
            </div>
        </div>
    </div>
</div>
@endsection