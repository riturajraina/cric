@extends('layouts.app')
@section('content')
@include('errors.register')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #2579A9;text-align: center;color:#E2FFFF">
                    <strong>Register</strong>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label for="fname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control" name="fname" 
                                       value="{{ old('fname') }}" required autofocus maxlength="25">

                                @if ($errors->has('fname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                            <label for="lname" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control" name="lname" 
                                       value="{{ old('lname') }}" required autofocus maxlength="25">

                                @if ($errors->has('lname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">User Name</label>
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" 
                                       name="username" value="{{ old('username') }}" required maxlength="100">

                                @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" 
                                       name="password" value="{{ old('password') }}" required maxlength="20">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('confpassword') ? ' has-error' : '' }}">
                            <label for="confpassword" class="col-md-4 control-label">
								Confirm Password
							</label>
                            <div class="col-md-6">
                                <input id="confpassword" type="password" class="form-control" 
                                       name="confpassword" value="{{ old('confpassword') }}" required maxlength="20">

                                @if ($errors->has('confpassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('confpassword') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                            <label for="captcha" class="col-md-4 control-label">
                                Please enter characters as shown
                            </label>
                            <div class="col-md-3">
                                <script language="javascript">
                                    function refreshCaptcha()
                                    {
                                        var cImg = document.getElementById('captchaImage');
                                        cImg.src = "<?php echo env('APP_URL') . '/captcha?i='; ?>" + 
                                                Math.floor((Math.random() * 100) + 1);
                                    }
                                </script>
                                <img id="captchaImage" src="<?php echo env('APP_URL') . '/captcha'; ?>">
                                <a href="javascript:void(0);" onclick="javascript:refreshCaptcha();">Refresh</a>
                                <input id="captcha" type="text" class="form-control" name="captcha" 
                                       value="{{ old('captcha') }}" required maxlength="5">

                                @if ($errors->has('captcha'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('captcha') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
