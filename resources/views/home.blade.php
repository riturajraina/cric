@extends('layouts.app')

@section('content')
@include('errors.common')
<?php
    $numberOfcolumns = 8;
?>
<div class="container" style="width:100%;">
    <div class="row"><!--style="width: 1500px;"-->
        <div class="col-md-30">
            <div class="alert alert-success">
                <table width="100%" align="center" border = "1" style="border-collapse:separate;border-spacing:2px;" 
                rules = "none" cellspacing="2" cellpadding="4" rules="1">
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" align="center" 
                            style="background-color: #2579A9;text-align: center;color:#E2FFFF">
                            <h1>
                                Team List
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" width="100%" align="center">
                            <a href="javascript:void(0);" title="Click here to create new team">
                                <strong>Create New Team</strong>
                            </a>
                        </td>
                    </tr>
                    <tr style="background-color:white">
                        <td width="4%" align="center">
                            <strong>S.No.</strong>
                        </td>
                        
                        <td width="13%">
                            <strong>Identifier</strong>
                        </td>
                        
                        <td width="15%">
                            <strong>Name</strong>
                        </td>
                        
                        <td width="20%" style="text-align: center;">
                            <strong>Logo</strong>
                        </td>
                        
                        <td width="15%">
                            <strong>Club State</strong>
                        </td>
                        
                        <td width="11%">
                            <strong>Created At</strong>
                        </td>
                        <td width="11%">
                            <strong>Updated At</strong>
                        </td>
                        
                        <td width="3%">
                            <strong>Status</strong>
                        </td>
                        
                        
                    </tr>
                    <?php
                        if (!count($teamList) || empty($teamList)) {
                    ?>
                            <tr>
                                <td colspan="<?php echo $numberOfcolumns;?>" width="100%" align="center">
                                    No Teams found
                                </td>
                            </tr>
                    <?php
                        } else {
                            
                            $i = 1;
                            
                            if (!empty($_REQUEST['page'])) {
                                $i = (($_REQUEST['page']-1) * env('RECORDS_PER_PAGE')) + 1;
                            }
                            
                            
                            foreach ($teamList as $row) {
                                
                                $row = $row->getAttributes();
                                
                                //echo '<br>Row : <pre>' . print_r($row, true) . '</pre>';exit;
                    ?>          
                                <tr>
                                    <td style="text-align: center;" valign="middle">
                                        <strong><?php echo $i++;?>.</strong>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" 
                                         title="Click here to edit this team">
                                            <strong>
                                                <?php echo $row['identifier'];?>
                                            </strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/viewplayers/<?php echo $row['pk_cri_team'];?>" 
                                         target="_blank" 
                                         title="Click here to view this team players">
                                            <strong>
                                                <?php echo $row['name'];?>
                                            </strong>
                                        </a>
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="<?php echo $row['logo_uri'];?>"
                                           target="_blank">
                                            <img src="<?php echo $row['logo_uri'];?>"
                                                 height="50" width="50">
                                        </a>
                                    </td>
                                    
                                    <td>
                                        <?php echo $row['club_state'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['created_at'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['updated_at'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['status'];?>
                                    </td>
                                    
                                </tr>
                    <?php
                            }
                        }
                    ?>
                    
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" align="center">
                            <?php
                                if (!empty($teamList)) {
                                    echo $teamList->appends($_REQUEST)->links();
                                }
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection