@extends('layouts.app')
@section('content')
@include('errors.common')
<?php
    $numberOfcolumns = 2;
?>
<div class="container" style="width:100%;">
    <div class="row"><!--style="width: 1500px;"-->
        <div class="col-md-30">
            <div class="alert alert-success">
                <form name="matchesForm" method="post" action="matches">
                    {{ csrf_field() }}
                    <table width="50%" align="center" border = "1" style="border-collapse:separate;border-spacing:2px;" 
                    rules = "none" cellspacing="2" cellpadding="4" rules="1">
                        <tr>
                            <td colspan="<?php echo $numberOfcolumns;?>" align="center" 
                                style="background-color: #2579A9;text-align: center;color:#E2FFFF">
                                <h1>
                                    View Matches between Teams
                                </h1>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="<?php echo $numberOfcolumns;?>" width="100%" align="center">
                                <a href="javascript:void(0);" title="Click here to create new match between teams (dummy)">
                                    <strong>Create New Match</strong>
                                </a>
                            </td>
                        </tr>
                        <tr style="background-color:white">
                            <td width="50%" align="right">
                                <strong>Select First Team : &nbsp;</strong>
                            </td>

                            <td width="50%">
                                <select name="firstTeamId" id="firstTeamId">
                                    <?php
                                        echo $selectOptionsManager->showSelectOptions($teamList, $firstTeamId);
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr style="background-color:white">
                            <td align="right">
                                <strong>Select Second Team : &nbsp;</strong>
                            </td>

                            <td>
                                <select name="secondTeamId" id="secondTeamId">
                                    <?php
                                        echo $selectOptionsManager->showSelectOptions($teamList, $secondTeamId);
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="<?php echo $numberOfcolumns;?>" style="text-align: center;">
                                <input type="submit" name="searchMatches" value="Search">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php
        $numberOfcolumnsList = 8;
    ?>
    <div class="row"><!--style="width: 1500px;"-->
        <div class="col-md-30">
            <div class="alert alert-success">
                <table width="100%" align="center" border = "1" 
                       style="border-collapse:separate;border-spacing:1px;border-color: black;" 
                       rules = "all" cellspacing="2" cellpadding="4">
                    <tr>
                        <td colspan="<?php echo $numberOfcolumnsList;?>" align="center" 
                            style="background-color: #2579A9;text-align: center;color:#E2FFFF">
                            <h1>
                                View Matches between Teams
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td width="2%" align="center">
                            <strong>S.No.</strong>
                        </td>
                        
                        <td width="14%">
                            <strong>First Team (Winner)</strong>
                        </td>
                        
                        <td width="14%">
                            <strong>Second Team</strong>
                        </td>
                        
                        <td width="14%" style="text-align: center;">
                            <strong>First Team Points</strong>
                        </td>
                        
                        <td width="14%" style="text-align: center;">
                            <strong>Second Team Points</strong>
                        </td>
                        
                        <td width="14%" style="text-align: center;">
                            <strong>Match Date</strong>
                        </td>
                        
                        <td width="14%">
                            <strong>Created At</strong>
                        </td>
                        <td width="14%">
                            <strong>Updated At</strong>
                        </td>
                    </tr>
                    
                    <?php
                        if (!count($matchList) || empty($matchList)) {
                    ?>
                            <tr>
                                <td colspan="<?php echo $numberOfcolumnsList;?>" width="100%" align="center">
                                    No Matches found
                                </td>
                            </tr>
                    <?php
                        } else {
                            
                            $i = 1;
                            
                            if (!empty($_REQUEST['page'])) {
                                $i = (($_REQUEST['page']-1) * env('RECORDS_PER_PAGE')) + 1;
                            }
                            
                            foreach ($matchList as $row) {
                                
                                $row = $row->getAttributes();
                                
                    ?>          
                                <tr>
                                    <td style="text-align: center;" valign="middle">
                                        <strong><?php echo $i++;?>.</strong>
                                    </td>
                                    <td>
                                        <?php echo $teamList[$row['fk_cri_team_first']];?>
                                    </td>
                                    <td>
                                        <?php echo $teamList[$row['fk_cri_team_second']];?>
                                    </td>
                                    
                                    <td style="text-align: center;" valign="middle">
                                        <?php echo $row['first_team_point'];?>
                                    </td>
                                    
                                    <td style="text-align: center;" valign="middle">
                                        <?php echo $row['second_team_point'];?>
                                    </td>
                                    
                                    <td style="text-align: center;">
                                        <?php echo $row['match_day'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['created_at'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['updated_at'];?>
                                    </td>
                                </tr>
                    <?php
                            }
                        }
                    ?>
                    
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" align="center">
                            <?php
                                if (!empty($matchList)) {
                                    echo $matchList->appends($_REQUEST)->links();
                                }
                            ?>
                        </td>
                    </tr>
                           
                </table>
            </div>
        </div>
    </div>
</div>
@endsection