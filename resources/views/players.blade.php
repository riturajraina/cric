@extends('layouts.app')

@section('content')
@include('errors.common')
<?php
    $numberOfcolumns = 9;
?>
<div class="container" style="width:100%;">
    <div class="row"><!--style="width: 1500px;"-->
        <div class="col-md-30">
            <div class="alert alert-success">
                <table width="100%" align="center" border = "1" style="border-collapse:separate;border-spacing:2px;" 
                rules = "none" cellspacing="2" cellpadding="4" rules="1">
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" align="center" 
                            style="background-color: #2579A9;text-align: center;color:#E2FFFF">
                            <h1>
                                Players List for Team - <?php echo $team;?>
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" width="100%" align="center">
                            <a href="javascript:void(0);" title="Click here to create new player">
                                <strong>Create New Player</strong>
                            </a>
                        </td>
                    </tr>
                    <tr style="background-color:white">
                        <td width="4%" align="center">
                            <strong>S.No.</strong>
                        </td>
                        
                        <td width="12%">
                            <strong>Identifier</strong>
                        </td>
                        
                        <td width="15%">
                            <strong>First Name</strong>
                        </td>
                        
                        <td width="14%">
                            <strong>Last Name</strong>
                        </td>
                        
                        <td width="14%" style="text-align: center;">
                            <strong>Image</strong>
                        </td>
                        
                        <td width="10%">
                            <strong>Jersey Number</strong>
                        </td>
                        
                        <td width="10%">
                            <strong>Created At</strong>
                        </td>
                        <td width="10%">
                            <strong>Updated At</strong>
                        </td>
                        
                        <td width="3%">
                            <strong>Status</strong>
                        </td>
                        
                        
                    </tr>
                    <?php
                        if (!count($playerList) || empty($playerList)) {
                    ?>
                            <tr>
                                <td colspan="<?php echo $numberOfcolumns;?>" width="100%" align="center">
                                    No Players found for this Team
                                </td>
                            </tr>
                    <?php
                        } else {
                            
                            $i = 1;
                            
                            if (!empty($_REQUEST['page'])) {
                                $i = (($_REQUEST['page']-1) * env('RECORDS_PER_PAGE')) + 1;
                            }
                            
                            
                            foreach ($playerList as $row) {
                                
                                $row = $row->getAttributes();
                                
                                //echo '<br>Row : <pre>' . print_r($row, true) . '</pre>';exit;
                    ?>          
                                <tr>
                                    <td style="text-align: center;" valign="middle">
                                        <strong><?php echo $i++;?>.</strong>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" 
                                         title="Click here to edit this player">
                                            <strong><?php echo $row['identifier'];?></strong>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/playerhistory/<?php echo $row['pk_cri_player'];?>" 
                                         title="Click here to view this player's history">
                                            <strong>
                                                <?php echo $row['first_name'];?>
                                            </strong>
                                        </a>
                                    </td>
                                    
                                    <td>
                                        <strong>
                                            <?php echo $row['last_name'];?>
                                        </strong>
                                    </td>
                                    
                                    <td style="text-align: center;">
                                        <a href="<?php echo $row['image_uri'];?>"
                                           target="_blank">
                                            <img src="<?php echo $row['image_uri'];?>"
                                                 height="50" width="50">
                                        </a>
                                    </td>
                                    
                                    <td>
                                        <?php echo $row['jersey_number'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['created_at'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['updated_at'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['status'];?>
                                    </td>
                                    
                                </tr>
                    <?php
                            }
                        }
                    ?>
                    
                    <tr>
                        <td colspan="<?php echo $numberOfcolumns;?>" align="center">
                            <?php
                                if (!empty($playerList)) {
                                    echo $playerList->appends($_REQUEST)->links();
                                }
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection