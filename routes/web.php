<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/playerhistory/{playerid}', 'PlayerController@viewPlayerHistory');

Route::post('/matches', 'MatchesController@viewMatches');

Route::get('/matches', 'MatchesController@viewMatches');

Route::get('/viewplayers/{teamid}', 'PlayerController@viewPlayers');

Route::get('/', 'HomeController@home');

Route::get('/home', 'HomeController@home');

Route::get('/captcha/{random}', 'CaptchaController@index');

Route::get('/captcha', 'CaptchaController@index');

Route::get('/success/{message}', 'ErrorController@showError');

Route::get('/goterror/{message}', 'ErrorController@showError');

